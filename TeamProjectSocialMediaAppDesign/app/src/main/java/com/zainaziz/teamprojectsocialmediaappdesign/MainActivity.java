package com.zainaziz.teamprojectsocialmediaappdesign;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.zainaziz.teamprojectsocialmediaappdesign.Adapters.CustomViewPager;
import com.zainaziz.teamprojectsocialmediaappdesign.Fragments.FragmentFollowers;
import com.zainaziz.teamprojectsocialmediaappdesign.Fragments.FragmentFollowing;
import com.zainaziz.teamprojectsocialmediaappdesign.Fragments.FragmentSuggest;

public class MainActivity extends AppCompatActivity {
    ViewPager viewPager;
    CustomViewPager customViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //initialize widgets
        viewPager=findViewById(R.id.viewPager);

        customViewPager=new CustomViewPager(getSupportFragmentManager());

        //add fragment to viewpager
        customViewPager.addFragment(new FragmentFollowers());
        customViewPager.addFragment(new FragmentFollowing());
        customViewPager.addFragment(new FragmentSuggest());

        viewPager.setAdapter(customViewPager);
    }
}