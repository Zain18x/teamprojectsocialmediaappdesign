package com.zainaziz.teamprojectsocialmediaappdesign.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class CustomViewPager extends FragmentPagerAdapter {

    private ArrayList<Fragment> mFragmentList;
    private ArrayList<String> mNameList;
    public CustomViewPager(@NonNull FragmentManager fm) {
        super(fm);
        mFragmentList= new ArrayList<>();
        mNameList=new ArrayList<>();
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }
public void addFragment(Fragment fragment){
        mFragmentList.add(fragment);
}

}
